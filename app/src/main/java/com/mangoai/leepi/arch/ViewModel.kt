package com.mangoai.leepi.arch

import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import com.mangoai.leepi.BR

interface ViewModel

interface ViewModelBinder {
    fun bind(binding: ViewDataBinding, vm: ViewModel)

    companion object {
        val defaultBinder = viewModelBinder { binding, vm -> binding.setVariable(BR.vm, vm) }
    }
}

fun viewModelBinder(binder: (ViewDataBinding, ViewModel) -> Unit): ViewModelBinder =
    object : ViewModelBinder {
        override fun bind(binding: ViewDataBinding, vm: ViewModel) = binder(binding, vm)
    }

interface ViewProvider {
    @LayoutRes
    fun getView(vm: ViewModel): Int
}

fun viewProvider(provider: (ViewModel) -> Int): ViewProvider =
    object: ViewProvider {
        override fun getView(vm: ViewModel): Int = provider(vm)
    }