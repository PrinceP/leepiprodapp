package com.mangoai.leepi.arch;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableList;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import static com.mangoai.leepi.arch.ViewModelAdapter.DataBindingViewHolder;

public class ViewModelAdapter<T extends ViewModel> extends RecyclerView.Adapter<DataBindingViewHolder> {
    protected final ObservableList<T> viewModels;
    private final ViewProvider viewProvider;
    private final ViewModelBinder binder;
    private final SimpleObservableListCallback<T> callback;

    public ViewModelAdapter(ObservableList<T> viewModels, ViewProvider viewProvider,
                            ViewModelBinder viewModelBinder) {
        this.viewModels = viewModels;
        this.viewProvider = viewProvider;
        this.binder = viewModelBinder;
        callback = new SimpleObservableListCallback<>(this);
        this.viewModels.addOnListChangedCallback(callback);
    }

    @Override
    public DataBindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return DataBindingViewHolder.create(parent, viewType);
    }

    @Override
    public void onBindViewHolder(DataBindingViewHolder holder, int position) {
        T vm = getObjForPosition(position);
        ViewModelBinder.Companion.getDefaultBinder().bind(holder.binding, vm);
        binder.bind(holder.binding, vm);
        holder.binding.executePendingBindings();
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        viewModels.removeOnListChangedCallback(callback);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull DataBindingViewHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull DataBindingViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
    }

    @Override
    public int getItemCount() {
        return viewModels.size();
    }

    protected T getObjForPosition(int position) {
        return viewModels.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        return viewProvider.getView(getObjForPosition(position));
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public static class DataBindingViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
        private final T binding;

        public T getBinding() {
            return binding;
        }

        private DataBindingViewHolder(T binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private static <T extends ViewDataBinding> DataBindingViewHolder<T> create(ViewGroup parent,
                                                                                   @LayoutRes int layoutId) {
            T binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    layoutId, parent, false);
            return new DataBindingViewHolder<>(binding);
        }
    }
}
