package com.mangoai.leepi.permissions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.mangoai.leepi.R
import com.mangoai.leepi.databinding.DialogCameraPermissionBinding
import com.mangoai.leepi.home.HomeActivity

class CameraPermissionDialog: DialogFragment() {
    companion object {
        fun newInstance(showSettingsCTA: Boolean = false): CameraPermissionDialog {
            val bundle = Bundle()
            bundle.putBoolean("showSettingsCTA", showSettingsCTA)
            return CameraPermissionDialog().apply {
                arguments = bundle
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_FloatingDialog_Black)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DialogCameraPermissionBinding.inflate(inflater)
        val showSettingsCTA = arguments?.getBoolean("showSettingsCTA") ?: false
        if (showSettingsCTA) {
            binding.buttonAllow.text = getString(R.string.open_settings)
        }
        binding.close.setOnClickListener {
            dismiss()
        }
        binding.buttonAllow.setOnClickListener {
            if (showSettingsCTA) {
                (activity as? HomeActivity)?.openSettingsPage?.invoke()
            } else {
                (activity as? HomeActivity)?.onCameraPermissionAllow?.invoke()
            }
            dismiss()
        }
        return binding.root
    }
}
