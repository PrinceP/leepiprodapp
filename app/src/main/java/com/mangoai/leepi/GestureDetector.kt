package com.mangoai.leepi

import android.content.Context
import android.content.res.AssetManager
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks.call
import org.tensorflow.lite.Interpreter
import timber.log.Timber
import java.io.FileInputStream
import java.io.IOException
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class GestureDetector(private val context: Context) {
    private var interpreter: Interpreter? = null
    var isInitialized = false
        private set

    /** Executor to run inference task in the background */
    private val executorService: ExecutorService = Executors.newCachedThreadPool()

    private var inputImageWidth: Int = 0 // will be inferred from TF Lite model
    private var inputImageHeight: Int = 0 // will be inferred from TF Lite model
    private var modelInputSize: Int = 0 // will be inferred from TF Lite model

    private val classNames = listOf("Book", "Candy", "Clothes", "Computer", "Drink", "Want")

    fun initialize(): Task<Void> {
        return call(
            executorService,
            Callable<Void> {
                initializeInterpreter()
                null
            }
        )
    }

    @Throws(IOException::class)
    private fun initializeInterpreter() {
        // Load the TF Lite model
        val assetManager = context.assets
        val model = loadModelFile(assetManager)


//        val delegate = GpuDelegate()


        // Initialize TF Lite Interpreter with NNAPI enabled
        val options = Interpreter.Options()
        options.setUseNNAPI(false)
//        options.addDelegate(delegate)

        val interpreter = Interpreter(model, options)

        // Read input shape from model file
        val inputShape = interpreter.getInputTensor(0).shape()
        inputImageWidth = inputShape[1]
        inputImageHeight = inputShape[2]
//        modelInputSize = FLOAT_TYPE_SIZE * inputImageWidth * inputImageHeight * PIXEL_SIZE
        modelInputSize = FLOAT_TYPE_SIZE * TIME * FEATURES

        // Finish interpreter initialization
        this.interpreter = interpreter
        isInitialized = true
        Timber.d("Initialized TFLite interpreter.")
    }

    @Throws(IOException::class)
    private fun loadModelFile(assetManager: AssetManager): ByteBuffer {
        val fileDescriptor = assetManager.openFd(MODEL_FILE)
        val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
        val fileChannel = inputStream.channel
        val startOffset = fileDescriptor.startOffset
        val declaredLength = fileDescriptor.declaredLength
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
    }

    private fun toFloatArray(list: List<List<Float>>): FloatArray? {
        var i = 0
        val array = FloatArray(TIME * FEATURES + 1)
        println("Size of data = " + array.size)
        for (f in list) {
//            array[i++] = f ?: Float.NaN
            for (f_ in f) {
                array[i++] = f_ ?: Float.NaN
            }
        }
        return array
    }

    fun classifygesture(input: List<List<Float>>): String {
        if (!isInitialized) {
            throw IllegalStateException("TF Lite Interpreter is not initialized yet.")
        }
        var startTime: Long
        var elapsedTime: Long

        // Preprocessing: resize the input
        startTime = System.nanoTime()

        elapsedTime = (System.nanoTime() - startTime) / 1000000
        Timber.d("Preprocessing time = " + elapsedTime + "ms")

        startTime = System.nanoTime()
        val result = Array(1) { FloatArray(OUTPUT_CLASSES_COUNT) }
        interpreter?.run(toFloatArray(input), result)
        elapsedTime = (System.nanoTime() - startTime) / 1000000
        Timber.d("Inference time = " + elapsedTime + "ms")

        return getOutputString(result[0])
    }

    fun classifyAsync(input: MutableList<List<Float>>): Task<String> {
        return call(executorService, Callable<String> { classifygesture(input) })
    }

    fun close() {
        call(
            executorService,
            Callable<String> {
                interpreter?.close()
                Timber.d("Closed TFLite interpreter.")
                null
            }
        )
    }

    private fun getOutputString(output: FloatArray): String {
        val maxIndex = output.indices.maxBy { output[it] } ?: -1
        /*return "Prediction Result: %d\nConfidence: %2f\nClass: %s".format(
            maxIndex,
            output[maxIndex],
            classNames[maxIndex]
        )*/
        return classNames[maxIndex]
    }

    companion object {
        private const val TAG = "GestureClassifier"
        private const val MODEL_FILE = "one_15.tflite"
        private const val FLOAT_TYPE_SIZE = 1
        private const val TIME = 15
        private const val FEATURES = 128
        private const val OUTPUT_CLASSES_COUNT = 6
    }
}