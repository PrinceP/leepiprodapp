package com.mangoai.leepi.camera;

import com.google.mediapipe.components.CameraXPreviewHelper;

public class CameraXHelper extends CameraXPreviewHelper {

    @Override
    public boolean isCameraRotated() {
        return false;
    }
}
