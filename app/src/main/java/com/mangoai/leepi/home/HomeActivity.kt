package com.mangoai.leepi.home

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.mediapipe.components.PermissionHelper
import com.mangoai.leepi.R
import com.mangoai.leepi.arch.ViewModelAdapter
import com.mangoai.leepi.arch.viewModelBinder
import com.mangoai.leepi.arch.viewProvider
import com.mangoai.leepi.classroom.CurvedItemDecoration
import com.mangoai.leepi.databinding.ActivityHomeBinding
import com.mangoai.leepi.databinding.ItemModuleColumnBinding
import com.mangoai.leepi.permissions.CameraPermissionDialog
import com.mangoai.leepi.util.DataStore
import timber.log.Timber


class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var vm: HomeViewModel
    private val ds = DataStore(this)
    private var moduleId: Int = 0

    val onCameraPermissionAllow: () -> Unit = {
        PermissionHelper.checkAndRequestCameraPermissions(this)
    }

    val openSettingsPage: () -> Unit = {
        val intent =
            Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        vm = HomeViewModel(ds)
    }


    private val viewProvider = viewProvider {
        when (it) {
            is ModuleColumnVm -> R.layout.item_module_column
            else -> R.layout.item_module_column
        }
    }

    private val viewModelBinder = viewModelBinder { viewDataBinding, _ ->
        when (viewDataBinding) {
            is ItemModuleColumnBinding -> {
                viewDataBinding.onModuleClick = onModuleClick
            }
        }
    }

    private val onModuleClick = { moduleCardVm: ModuleCardVm ->
        if (moduleCardVm.unlocked) {
            if (PermissionHelper.cameraPermissionsGranted(this)) {
                ModuleTipsDialogFragment.newInstance(moduleCardVm.id, ds)
                    .show(supportFragmentManager, "Module Tips")
            } else {
                moduleId = moduleCardVm.id
                CameraPermissionDialog.newInstance()
                    .show(supportFragmentManager, "Camera Permission")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        vm.clear()
    }

    override fun onResume() {
        super.onResume()
        vm = HomeViewModel(ds)
        val viewModelAdapter = ViewModelAdapter(vm.moduleColumnVms, viewProvider, viewModelBinder)
        binding.modulesGrid.adapter = viewModelAdapter
        binding.modulesGrid.addItemDecoration(
            CurvedItemDecoration(
                ContextCompat.getColor(
                    this,
                    R.color.black_alpha
                ),
                ContextCompat.getColor(
                    this,
                    R.color.black_alpha_2
                )
            )
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (permissions.isNotEmpty() && grantResults.size != permissions.size) {
            Timber.d("Permission denied.")
        } else {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    Timber.d( "${permissions[i]} + permission granted.")
                    ModuleTipsDialogFragment.newInstance(moduleId, ds)
                        .show(supportFragmentManager, "Module Tips")
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        val showRationale = shouldShowRequestPermissionRationale(permissions[i])
                        if (!showRationale) {
                           CameraPermissionDialog.newInstance(true)
                               .show(supportFragmentManager, "Camera Permission")
                        }
                    }
                }
            }
        }
    }
}
