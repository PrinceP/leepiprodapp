package com.mangoai.leepi.home

import androidx.databinding.ObservableArrayList
import com.mangoai.leepi.ModulesDataStore
import com.mangoai.leepi.arch.ViewModel
import com.mangoai.leepi.util.DataStore
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class HomeViewModel(val ds: DataStore) : ViewModel {
    private val disposables = CompositeDisposable()
    private val moduleCardVms = ArrayList<ModuleCardVm>()
    val moduleColumnVms = ObservableArrayList<ModuleColumnVm>()


    init {
        loadModules()
    }

    private fun loadModules() {
        disposables += Single.just(ModulesDataStore.loadModulesJson())
            .flattenAsObservable { it.moduleList }
            .map { ModuleCardVm(it, ds) }
            .toList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                moduleCardVms.addAll(it)
                for (i in 0 until moduleCardVms.size step 2) {
                    moduleColumnVms.add(ModuleColumnVm(moduleCardVms[i], moduleCardVms[i+1]))
                }
            }, { Timber.e(it) })
    }

    fun clear() {
        disposables.clear()
    }
}