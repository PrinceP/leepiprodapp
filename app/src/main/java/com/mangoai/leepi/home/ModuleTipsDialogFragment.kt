package com.mangoai.leepi.home

import android.opengl.Visibility
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.mangoai.leepi.ModulesDataStore
import com.mangoai.leepi.R
import com.mangoai.leepi.classroom.ClassroomActivity
import com.mangoai.leepi.databinding.FragmentModuleTipsDialogBinding
import com.mangoai.leepi.util.DataStore

class ModuleTipsDialogFragment: DialogFragment() {
    private lateinit var binding: FragmentModuleTipsDialogBinding
    private val ds = null
    companion object {
        fun newInstance(moduleId: Int, ds: DataStore): ModuleTipsDialogFragment {
            val bundle = Bundle()
            bundle.putInt("moduleId", moduleId)
            val progressValue = ds.getData(moduleId.toString())
            bundle.putInt("progressValue", progressValue.toInt())
            return ModuleTipsDialogFragment().apply {
                arguments = bundle
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.AppTheme_FloatingDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentModuleTipsDialogBinding.inflate(inflater)

        val moduleId = arguments?.getInt("moduleId") ?: -1
        val progressValue = arguments?.getInt("progressValue") ?: 0
        val module = ModulesDataStore.loadModule(moduleId)
        val lessons = module?.lessons
        val lessonSize = lessons?.size ?: 0
        val lessonId = Math.ceil((progressValue.toFloat() / 100.0) * lessonSize.toFloat())
        val completionText = "${lessonId.toInt()}/$lessonSize completed"

        binding.moduleImage = ModulesDataStore.moduleImage(moduleId)
        binding.moduleProgress.setProgress(progressValue)
        binding.textModuleName.setText(ModulesDataStore.moduleName(moduleId))
        binding.textLessonComplete.setText(completionText)

        if(progressValue == 0){
            binding.buttomTips.visibility = View.GONE
        }
        else if(progressValue == 100){
            binding.buttonNextRound.visibility = View.GONE
        }
        binding.buttomTips.setOnClickListener {
//            Toast.makeText(this.context, "Work in Progress.", Toast.LENGTH_SHORT).show()
            context?.let {
                if (moduleId > -1) {
                    startActivity(ClassroomActivity.createIntent(it, moduleId))
                }
            }
            dismiss()
        }
        binding.buttonNextRound.setOnClickListener {
            context?.let {
                if (moduleId > -1) {
                    startActivity(ClassroomActivity.createIntent(it, moduleId, lessonId.toInt()))
                }
            }
            dismiss()
        }
        return binding.root
    }
}