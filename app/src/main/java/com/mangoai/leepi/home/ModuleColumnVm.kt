package com.mangoai.leepi.home

import com.mangoai.leepi.ModulesDataStore
import com.mangoai.leepi.arch.ViewModel
import com.mangoai.leepi.models.Module
import com.mangoai.leepi.util.DataStore

class ModuleColumnVm(val moduleCardTop: ModuleCardVm, val moduleCardBottom: ModuleCardVm) : ViewModel

class ModuleCardVm(val module: Module, ds: DataStore) : ViewModel {
    val id = module.id
    val title = module.title
    val unlocked = !module.locked
    val imageRes = ModulesDataStore.moduleImage(module.id)
    val imageBackgroundColor = ModulesDataStore.moduleImageBgColor(module.id)
    val lessons = module.lessons
    val progressValue = ds.getData(module.id.toString()).toInt()
}