package com.mangoai.leepi.util

import android.animation.*
import android.content.Context
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.OvershootInterpolator
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat

fun animateColor(context: Context, view: CardView, fromColor: Int, toColor: Int) {
    val colorFrom = ContextCompat.getColor(
        context,
        fromColor
    )
    val colorTo = ContextCompat.getColor(
        context,
        toColor
    )
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
    colorAnimation.addUpdateListener {
        view.setCardBackgroundColor(it.animatedValue as Int)
    }
    colorAnimation.start()
}

fun animateHeight(view: View, fromHeightPercent: Float, toHeightPercent: Float): ValueAnimator {
    val heightAnimator = ValueAnimator.ofFloat(fromHeightPercent, toHeightPercent).setDuration(300)
    heightAnimator.interpolator = AccelerateDecelerateInterpolator()
    heightAnimator.addUpdateListener {
        val newHeightPercent = it.animatedValue as Float
        val layoutParams = view.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.matchConstraintPercentHeight = newHeightPercent
        view.layoutParams = layoutParams
        view.requestLayout()
    }
    return heightAnimator
}

fun animateAlpha(view: View): Animator =
    ObjectAnimator.ofFloat(view, View.ALPHA, 1f).apply {
        duration = 300
        interpolator = AccelerateDecelerateInterpolator()

    }

fun animateScaleY(view: View): Animator =
    ObjectAnimator.ofFloat(view, View.SCALE_Y, 0f, 1f).apply {
        duration = 300
        interpolator = DecelerateInterpolator()
    }

fun animateScale(view:View): Animator {
    val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X,0f, 1f)
    val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y,0f, 1f)
    return ObjectAnimator.ofPropertyValuesHolder(view, scaleX, scaleY).apply {
        duration = 350
        interpolator = OvershootInterpolator()
    }
}