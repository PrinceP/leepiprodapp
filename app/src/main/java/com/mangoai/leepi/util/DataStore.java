package com.mangoai.leepi.util;

import android.content.Context;
import android.content.SharedPreferences;
import static android.content.Context.MODE_PRIVATE;

public class DataStore {


    private Context context;

    public DataStore(Context context){
        this.context = context;
    }

    public void addData(String key, String value){
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getData(String key){
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        return  pref.getString(key, "0");
    }

    public void clearData(String key){
        SharedPreferences pref = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.apply();
    }

}
