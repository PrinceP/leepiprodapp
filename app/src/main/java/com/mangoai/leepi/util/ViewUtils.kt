package com.mangoai.leepi.util

import android.content.res.Resources


val Int.dpToPx: Float
    get() = (this * Resources.getSystem().displayMetrics.density)