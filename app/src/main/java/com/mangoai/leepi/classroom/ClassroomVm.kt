package com.mangoai.leepi.classroom

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import com.google.mediapipe.formats.proto.ClassificationProto.Classification
import com.google.mediapipe.formats.proto.ClassificationProto.ClassificationList
import com.google.mediapipe.formats.proto.LandmarkProto
import com.google.mediapipe.formats.proto.RectProto
import com.mangoai.leepi.R
import com.mangoai.leepi.arch.ViewModel
import com.mangoai.leepi.models.Content
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber

const val N_SAMPLES: Int = 15

class ClassroomVm(private val onClassificationSuccess: () -> Unit,
                  private val onGestureClassification: (List<List<Float>>) -> Unit) : ViewModel {
    val skipVisibility = ObservableBoolean()
    val nextVisibility = ObservableBoolean()
    val textColorRes = ObservableInt(R.color.solid_white)
    private val disposables = CompositeDisposable()
    private val classificationSubject = PublishSubject.create<Classification>()
    private val x = mutableListOf<Float>()
    private val y = mutableListOf<List<Float>>()

    var isGestureClassified = false
    var isClassified = false

    val lessonContent = mutableListOf<Content>()

    private var flag_luminance = false
    private var flag_handfar = false
    private var flag_handclose = false
    private val flag_handpresence = false


    internal fun observeClassificationChanges(multiHandClassifications: List<ClassificationList>) {
        if (multiHandClassifications.isEmpty()) return

        val classificationResult = multiHandClassifications[0].getClassification(0)
        classificationSubject.onNext(classificationResult)
    }

    internal fun classifyAlphabet(alphabet: String?) {
        alphabet?.let {
            if (it.isEmpty()) return
        }
        isClassified = false
        disposables += classificationSubject
            .buffer(15)
            .filter { it.isNotEmpty() }
            .map { it.filter { classification -> classification.label == alphabet } }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Timber.d("Classification count for $alphabet ${it.count()}")
                if (!isClassified && it.count() >= ClassroomActivity.CLASSIFICATION_SUCCESS_THRESHOLD) {
                    onClassificationSuccess.invoke()
                    isClassified = true
                }
            }, { Timber.e(it) })
    }

    internal fun getMultiHandLandmarksDebugString(multiHandLandmarks: List<LandmarkProto.NormalizedLandmarkList>): String? {
        if (multiHandLandmarks.isEmpty()) {
            return "No hand landmarks"
        }
        val multiHandLandmarksStr = "Number of hands detected: " + multiHandLandmarks.size + "\n"
        if (multiHandLandmarks.size == 1) {
            x.add(0.0f)
            for (landmark in multiHandLandmarks[0].landmarkList) {
                x.add(landmark.x)
                x.add(landmark.y)
                x.add(landmark.z)
            }
            x.add(1.0f)
            for (i in 0..62) {
                x.add(0.0f)
            }
            y.add(x)

        } else {
            x.add(0.0f)
            for (landmark in multiHandLandmarks[0].landmarkList) {
                x.add(landmark.x)
                x.add(landmark.y)
                x.add(landmark.z)
            }
            x.add(1.0f)
            for (landmark in multiHandLandmarks[1].landmarkList) {
                x.add(landmark.x)
                x.add(landmark.y)
                x.add(landmark.z)
            }
            y.add(x)

        }
        if (!isGestureClassified) {
            activityPrediction(y)
        }
        x.clear()
        return multiHandLandmarksStr
    }

    private fun activityPrediction(y: MutableList<List<Float>>) {
        if (y.size % N_SAMPLES == 0) {
            val z = y.takeLast(N_SAMPLES)
            onGestureClassification(z)
        }
        if (y.size == 60) {
            y.clear();
        }
    }

    internal fun getMultiHandDetectionsDebugString(multi_hand_rects: List<RectProto.NormalizedRect>): String? {
        if (multi_hand_rects.isEmpty()) {
            return "No hand detections"
        }
        var multiHandDetectionsStr =
            "Number of hands detected: " + multi_hand_rects.size + "\n"
        var handIndex = 0
        for (normalizedRect in multi_hand_rects) {
//            multiHandDetectionsStr += "\t#Hand Classifications for hand[" + handIndex + "]: " + normalizedRect.height * normalizedRect.width + "\n"
            if(normalizedRect.height * normalizedRect.width > 0.0   && normalizedRect.height * normalizedRect.width < 0.20){
                flag_handfar = true
                flag_handclose = false
            }
            else if( normalizedRect.height * normalizedRect.width > 0.40){
                flag_handfar = false
                flag_handclose = true
            }
            handIndex = handIndex + 1
        }
        return multiHandDetectionsStr
    }

    internal fun getLuminanceValue(luminance: Float){
        flag_luminance = luminance < 110.0
    }

    internal fun judgeFeedback(): String {

        if(flag_luminance){
            flag_luminance = false
            return "Low Brightness"
        }
        if(flag_handclose){
            flag_handclose = false
            return "Hands are too close!"
        }
        if(flag_handfar){
            flag_handfar = false
            return "Hands are too far!"
        }
        return ""
    }

    internal fun clear() {
        disposables.dispose()
    }
}