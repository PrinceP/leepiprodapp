package com.mangoai.leepi.classroom

import android.animation.*
import android.content.Context
import android.content.Intent
import android.graphics.SurfaceTexture
import android.graphics.drawable.ClipDrawable
import android.graphics.drawable.LayerDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Size
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.mediapipe.components.CameraHelper
import com.google.mediapipe.components.ExternalTextureConverter
import com.google.mediapipe.components.FrameProcessor
import com.google.mediapipe.components.PermissionHelper
import com.google.mediapipe.formats.proto.ClassificationProto.ClassificationList
import com.google.mediapipe.formats.proto.LandmarkProto
import com.google.mediapipe.formats.proto.RectProto
import com.google.mediapipe.framework.Packet
import com.google.mediapipe.framework.PacketGetter
import com.google.mediapipe.glutil.EglManager
import com.mangoai.leepi.GestureDetector
import com.mangoai.leepi.ModulesDataStore
import com.mangoai.leepi.R
import com.mangoai.leepi.camera.CameraX2PreviewHelper
import com.mangoai.leepi.databinding.ActivityClassroomBinding
import com.mangoai.leepi.util.*
import timber.log.Timber


class ClassroomActivity : AppCompatActivity() {
    private val CAMERA_FACING = CameraHelper.CameraFacing.FRONT
    /**
     * {@link SurfaceTexture} where the camera-preview frames can be accessed
     */
    private var previewFrameTexture: SurfaceTexture? = null
    /**
     * {@link SurfaceView} that displays the camera-preview frames processed by a MediaPipe graph.
     */
    private lateinit var previewDisplayView: SurfaceView
    /**
     * Creates and manages an {@link EGLContext}.
     */
    private lateinit var eglManager: EglManager
    /**
     * Sends camera-preview frames into a MediaPipe graph for processing, and displays the processed
    frames onto a {@link Surface}.
     */
    private lateinit var processor: FrameProcessor
    /**
     * Converts the GL_TEXTURE_EXTERNAL_OES texture from Android camera into a regular texture to be
    consumed by {@link FrameProcessor} and the underlying MediaPipe graph.
     */
    private var convertor: ExternalTextureConverter? = null
    /**
     * Handles camera access via the {@link CameraX} Jetpack support library.
     */
    private var cameraHelper: CameraX2PreviewHelper? = null

    private lateinit var binding: ActivityClassroomBinding
    private lateinit var vm: ClassroomVm

    private val timeAnimator = TimeAnimator()

    private val gestureDetector = GestureDetector(this)

    private var incrementor = 0

    private val ds = DataStore(this)

    private var lessonSize = 0

    private var imagecounter = 0

    private val onClassificationSuccess = {
        animateColor(this, binding.alphabetCard.surface, R.color.blue_900, R.color.teal_400)
        binding.alphabetCard.checkMark.visibility = View.VISIBLE
        binding.alphabetCard.checkMark.playAnimation()
        vm.textColorRes.set(R.color.teal_400)
        timeAnimator.cancel()
        vm.skipVisibility.set(false)

        runOnUiThread {
            binding.textTip.visibility = View.GONE;
        }
    }

    private val onGestureClassification = { buffer: List<List<Float>> ->
        val predictedGesture = gestureDetector.classifygesture(buffer)
        Timber.d("Prediction $predictedGesture ${vm.lessonContent[incrementor]}")
        if (vm.lessonContent[incrementor].title == predictedGesture) {
            runOnUiThread {
                vm.isGestureClassified = true
                onClassificationSuccess.invoke()
            }
        }
    }

    private val surfaceCallbacks = object : SurfaceHolder.Callback {
        override fun surfaceChanged(
            holder: SurfaceHolder?, format: Int, width: Int,
            height: Int
        ) {
            val viewSize = Size(width, height)
            val displaySize: Size = cameraHelper!!.computeDisplaySizeFromViewSize(viewSize)
            convertor!!.setSurfaceTextureAndAttachToGLContext(
                previewFrameTexture, displaySize.width,
                displaySize.height
            )
        }

        override fun surfaceDestroyed(holder: SurfaceHolder?) {
            processor.videoSurfaceOutput.setSurface(null)
        }

        override fun surfaceCreated(holder: SurfaceHolder?) {
            processor.videoSurfaceOutput.setSurface(holder?.surface)
        }

    }

    companion object {
        const val BINARY_GRAPH_NAME = "multihandtrackinggpu.binarypb"
        const val INPUT_VIDEO_STREAM_NAME = "input_video"
        const val OUTPUT_VIDEO_STREAM_NAME = "output_video"
        private const val OUTPUT_LANDMARKS_STREAM_NAME = "multi_hand_landmarks"
        private const val OUTPUT_CLASSIFICATIONS_STREAM_NAME = "multi_hand_gesture"
        private const val OUTPUT_LUMINANCE_STREAM_NAME = "luminance_value"
        private const val OUTPUT_DETECTIONS_STREAM_NAME = "multi_hand_rects_from_landmarks"
        /**
        Flips the camera-preview frames vertically before sending them into FrameProcessor to be
        processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
        This is needed because OpenGL represents images assuming the image origin is at the bottom-left
        corner, whereas MediaPipe in general assumes the image origin is at top-left.
         */
        const val FLIP_FRAMES_VERTICALLY = true

        const val CLASSIFICATION_SUCCESS_THRESHOLD = 10

        fun createIntent(ctx: Context, moduleId: Int, lessonId: Int = 0): Intent =
            Intent(ctx, ClassroomActivity::class.java).putExtra("moduleId", moduleId)
                .putExtra("lessonId", lessonId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_classroom
        )
        vm = ClassroomVm(onClassificationSuccess, onGestureClassification)
        binding.vm = vm
        val moduleId = intent.getIntExtra("moduleId", 0)
        val lessonId = intent.getIntExtra("lessonId", 0)
        val module = ModulesDataStore.loadModule(moduleId)
        val lessons = module?.lessons
        lessonSize = lessons?.size ?: 0
        vm.lessonContent.addAll(lessons?.get(lessonId)?.contents ?: emptyList())
        binding.alphabetCard.root.alpha = 0f
        binding.prediction = " "
        previewDisplayView = SurfaceView(this)
        eglManager = EglManager(null)
        processor = FrameProcessor(
            this, eglManager.nativeContext,
            BINARY_GRAPH_NAME,
            INPUT_VIDEO_STREAM_NAME,
            OUTPUT_VIDEO_STREAM_NAME
        )

        processor.addPacketCallback(OUTPUT_CLASSIFICATIONS_STREAM_NAME) { packet: Packet ->
            //Timber.d("Received multi-hand classifications packet.")
            val multiHandClassifications = PacketGetter.getProtoVector(
                packet,
                ClassificationList.parser()
            )
            vm.observeClassificationChanges(multiHandClassifications)
            //Timber.d("""[TS:${packet.timestamp}] ${getMultiHandClassificationsDebugString(multiHandClassifications)}""")
        }


        processor.addPacketCallback(
            OUTPUT_LANDMARKS_STREAM_NAME
        ) { packet: Packet ->
            Timber.d("Received multi-hand landmarks packet.")
            val multiHandLandmarks =
                PacketGetter.getProtoVector(
                    packet,
                    LandmarkProto.NormalizedLandmarkList.parser()
                )
            Timber.d(
                """[TS:${packet.timestamp}] ${vm.getMultiHandLandmarksDebugString(multiHandLandmarks)}"""
            )
        }

        processor.addPacketCallback(
            OUTPUT_LUMINANCE_STREAM_NAME
        ) { packet: Packet ->
            Timber.d("Received luminance packet.")
            val luminanceValue = PacketGetter.getFloat32(packet)
            Timber.d(
                """[TS:${packet.timestamp}] $luminanceValue"""
            )
        }


        processor.addPacketCallback(
            OUTPUT_DETECTIONS_STREAM_NAME
        ) { packet: Packet ->
            Timber.d( "Received rectangles packet.")

            imagecounter++;
            val multiHandRectangles: List<RectProto.NormalizedRect> =
                PacketGetter.getProtoVector(packet, RectProto.NormalizedRect.parser())
            vm.getMultiHandDetectionsDebugString(multiHandRectangles)
            if(imagecounter % 10 == 0){
                val feedback = vm.judgeFeedback()
                if(feedback.isNotEmpty())
                {
                    alertUser(feedback)
                    imagecounter = 0
                }else{
                    runOnUiThread {
                        binding.textTip.visibility = View.GONE;
                    }
                }
            }

        }


        processor.videoSurfaceOutput.setFlipY(FLIP_FRAMES_VERTICALLY)

        processor.preheat()

        gestureDetector.initialize()
            .addOnSuccessListener {
                Timber.i("Gesture Classifier initialized successfully")
            }
            .addOnFailureListener {
                Timber.e("Unable to initialize Gesture classifier")
            }

        PermissionHelper.checkAndRequestCameraPermissions(this)

        binding.prediction = vm.lessonContent[0].title
        binding.letterImage = ModulesDataStore.imageForLetter(predictionText()!!)

        binding.startCamera.setOnClickListener {
            if (binding.videoContainer.visibility == View.VISIBLE && binding.wordVideoView.isPlaying) {
                binding.wordVideoView.stopPlayback()
            }
            previewDisplayView.holder.removeCallback(surfaceCallbacks)
            setupPreviewDisplayView()
            if (PermissionHelper.cameraPermissionsGranted(this)) {
                startCamera()
            }
            it.visibility = View.GONE
            vm.classifyAlphabet(predictionText())
            showNext()
            Handler().postDelayed({
                vm.skipVisibility.set(true)
                // progressSkipButton()
            }, 1000)
        }
        animate()
        binding.buttonNext.setOnClickListener {
            onSkipOrNextButtonClick(moduleId, lessonId)
            runOnUiThread {
                binding.textTip.visibility = View.GONE;
            }
        }
        binding.buttonSkip.setOnClickListener {
            onSkipOrNextButtonClick(moduleId, lessonId)
            runOnUiThread {
                binding.textTip.visibility = View.GONE;
            }
        }
        binding.imgClose.setOnClickListener {
            finish()
        }
        binding.progressBar.setProgress( (incrementor / vm.lessonContent.size) * 100)

    }


    private fun onSkipOrNextButtonClick(moduleId: Int, lessonId: Int) {
        binding.previewDisplayLayout.removeView(previewDisplayView)
        convertor?.close()
        cameraHelper?.unbind()
        binding.startCamera.visibility = View.VISIBLE
        convertor = ExternalTextureConverter(eglManager.context)
        convertor!!.setFlipY(FLIP_FRAMES_VERTICALLY)
        convertor!!.setConsumer(processor)
        binding.alphabetCard.checkMark.visibility = View.GONE
        animateColor(this, binding.alphabetCard.surface, R.color.teal_400, R.color.blue_900)
        vm.nextVisibility.set(false)
        vm.skipVisibility.set(false)
        vm.isGestureClassified = false
        var progressValue = (((incrementor.toFloat() + 1) / (vm.lessonContent.size).toFloat()) * 100).toInt()
        binding.progressBar.progress = progressValue

        if (incrementor < vm.lessonContent.size - 1) {
            vm.textColorRes.set(R.color.solid_white)
            val content = vm.lessonContent[++incrementor]
            binding.prediction = content.title
            if ("word" == content.type) {
                binding.imageLearn.visibility = View.GONE
                binding.videoContainer.visibility = View.VISIBLE
                playWordVideo(content.title)
            } else {
                binding.imageLearn.visibility = View.VISIBLE
                binding.videoContainer.visibility = View.GONE
                binding.letterImage = ModulesDataStore.imageForLetter(content.title)
                animateLetterImage().start()
            }
        } else {
            val moduleProgress = (((lessonId.toFloat() + 1) / (lessonSize).toFloat()) * 100).toInt()
            ds.addData(moduleId.toString(),moduleProgress.toString())
            startActivity(CompletionActivity.createIntent(this, moduleId, lessonId))
            finish()
        }
    }

    private fun playWordVideo(word: String) {
        val videoPath = "android.resource://$packageName/${ModulesDataStore.animationForWord(word)}"
        val videoView = binding.wordVideoView
        if (videoView.isPlaying) {
            videoView.stopPlayback()
        }
        videoView.setVideoURI(Uri.parse(videoPath))
        videoView.setOnPreparedListener {
            it.isLooping = true
        }
    }

    private fun showNext() {
        binding.alphabetCard.checkMark.addAnimatorUpdateListener {
            val progress = (it.animatedValue as Float * 100).toInt()
            if (progress > 60) {
                vm.nextVisibility.set(true)
                binding.alphabetCard.checkMark.removeAllAnimatorListeners()
            }
        }
    }

    private fun predictionText(): String? = binding.prediction

    private fun animate() {
        val animateAlphabetCard = animateAlpha(binding.alphabetCard.root)
        val animateSheetHeight = animateHeight(binding.wordSheet, 0.99f, 0.33f)
        val animatorSet = AnimatorSet()
        animatorSet.playSequentially(
            animateLetsSpellTextEnter(),
            animateAlphabetCard,
            animateLetsSpellTextExit(),
            animateSheetHeight,
            animateLetterImage(),
            animateCameraButtonWithDelay()
        )
        animatorSet.start()
    }

    private fun animateLetsSpellTextEnter(): Animator {
        val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 0.4f, 1f)
        val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, 0.2f, 1f)
        return ObjectAnimator.ofPropertyValuesHolder(binding.textLetsSpell, scaleY, scaleX).apply {
            duration = 300
            interpolator = AccelerateDecelerateInterpolator()
        }
    }

    private fun animateLetsSpellTextExit(): Animator {
        val translateY = PropertyValuesHolder.ofFloat(View.TRANSLATION_Y, 1f, -50f)
        val alpha = PropertyValuesHolder.ofFloat(View.ALPHA, 0f)
        val animator =
            ObjectAnimator.ofPropertyValuesHolder(binding.textLetsSpell, translateY, alpha).apply {
                duration = 300
                interpolator = AccelerateDecelerateInterpolator()
            }
        animator.startDelay = 800
        return animator
    }

    private fun animateCameraButtonWithDelay(): Animator {
        val animateCameraAlpha = animateScaleY(binding.startCamera).apply {
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(p0: Animator?) {
                    binding.startCamera.visibility = View.VISIBLE
                    binding.wordVideoView.start()
                }
            })
        }
        animateCameraAlpha.startDelay = 250
        return animateCameraAlpha
    }

    private fun animateLetterImage(): Animator {
        return animateScale(binding.imageLearn)
    }

    override fun onResume() {
        super.onResume()
        if (cameraHelper?.isBound == false) startCamera()
        convertor = ExternalTextureConverter(eglManager.context)
        convertor!!.setFlipY(FLIP_FRAMES_VERTICALLY)
        convertor!!.setConsumer(processor)
        if (binding.videoContainer.visibility == View.VISIBLE) {
            binding.wordVideoView.resume()
        }
        binding.textTip.visibility = View.GONE
    }

    private fun startCamera() {
        cameraHelper = CameraX2PreviewHelper()
        cameraHelper!!.setOnCameraStartedListener {
            previewFrameTexture = it
            previewDisplayView.visibility = View.VISIBLE
        }
        cameraHelper!!.startCamera(this, CAMERA_FACING, null)
    }

    override fun onPause() {
        super.onPause()
        previewFrameTexture?.release()
        previewDisplayView.visibility = View.GONE
        convertor?.close()
        cameraHelper?.unbind()
        if (binding.videoContainer.visibility == View.VISIBLE && binding.wordVideoView.isPlaying) {
            binding.wordVideoView.pause()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun setupPreviewDisplayView() {
        previewDisplayView.visibility = View.GONE
        val container = binding.previewDisplayLayout
        container.addView(previewDisplayView)
        previewDisplayView.holder.addCallback(surfaceCallbacks)
    }

    private var currentProgress = 0

    private fun progressSkipButton() {
        val clipDrawable: ClipDrawable =
            (binding.buttonSkip.background as LayerDrawable).findDrawableByLayerId(R.id.clip_drawable) as ClipDrawable
        timeAnimator.setTimeListener { _, _, _ ->
            clipDrawable.level = currentProgress
            if (currentProgress >= 10000) {
                binding.buttonSkip.background =
                    resources.getDrawable(R.drawable.backround_rounded_blue, this.theme)
                timeAnimator.cancel()
            } else {
                currentProgress = 10000.coerceAtMost(currentProgress + 10)
            }
        }
        if (!timeAnimator.isRunning) {
            currentProgress = 0
            timeAnimator.start()
        }
    }

    private fun alertUser(feedback: String) {
        if(!vm.isGestureClassified and !vm.isClassified) {
            runOnUiThread {
                binding.textTip.visibility = View.VISIBLE
                binding.textTip.text = feedback
//                Toast.makeText(binding.root.context, feedback, Toast.LENGTH_SHORT).show()
            }


        }
    }

    private fun getMultiHandLandmarksDebugString(multiHandLandmarks: List<LandmarkProto.NormalizedLandmarkList>): String? {
        if (multiHandLandmarks.isEmpty()) {
            return "No hand landmarks"
        }
        return "Number of hands detected: " + multiHandLandmarks.size + "\n"
    }

    override fun onDestroy() {
        vm.clear()
        super.onDestroy()
    }
}
