package com.mangoai.leepi.classroom

import android.graphics.Canvas
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.Path
import androidx.recyclerview.widget.RecyclerView
import com.mangoai.leepi.util.dpToPx

class CurvedItemDecoration(enabled: Int, disabled: Int) : RecyclerView.ItemDecoration() {
    private val drawPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = enabled
        strokeWidth = 4.dpToPx
        style = Paint.Style.STROKE
        pathEffect = DashPathEffect(floatArrayOf(4.dpToPx, 8.dpToPx), 0f)
        strokeCap = Paint.Cap.ROUND
    }

    private val drawPaint2 = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = disabled
        strokeWidth = 4.dpToPx
        style = Paint.Style.STROKE
        pathEffect = DashPathEffect(floatArrayOf(4.dpToPx, 8.dpToPx), 0f)
        strokeCap = Paint.Cap.ROUND
    }


    override fun onDraw(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDraw(canvas, parent, state)

        for (childIndex in 0 until parent.childCount) {
            val childView = parent.getChildAt(childIndex)
            val dataIndex = parent.getChildAdapterPosition(childView)
            val paint = if (dataIndex < 2) drawPaint else drawPaint2
            if (dataIndex % 2 == 0) {
                canvas.drawPath(Path().apply {
                    moveTo(
                        childView.left.toFloat() + 20,
                        childView.top.toFloat() + 200
                    )
                    cubicTo(
                        childView.left.toFloat() + 150,
                        childView.top.toFloat(),
                        childView.left.toFloat() + 50,
                        childView.bottom.toFloat(),
                        childView.right.toFloat(),
                        childView.bottom.toFloat() + 40
                    )
                }, paint)
            } else {
                canvas.drawPath(Path().apply {
                    moveTo(
                        childView.right.toFloat(),
                        childView.top.toFloat() + 200
                    )
                    cubicTo(
                        childView.right.toFloat() - 260,
                        childView.top.toFloat(),
                        childView.right.toFloat(),
                        childView.bottom.toFloat(),
                        childView.left.toFloat() + 20,
                        childView.bottom.toFloat() + 40
                    )
                }, paint)
            }

        }

    }

}