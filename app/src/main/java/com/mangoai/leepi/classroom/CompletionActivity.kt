package com.mangoai.leepi.classroom

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.mangoai.leepi.ModulesDataStore
import com.mangoai.leepi.R
import com.mangoai.leepi.databinding.ActivityCompletionBinding
import com.mangoai.leepi.models.Module

class CompletionActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCompletionBinding

    companion object {
        fun createIntent(ctx: Context, moduleId: Int, lessonId: Int): Intent =
            Intent(ctx, CompletionActivity::class.java)
                .putExtra("moduleId", moduleId)
                .putExtra("lessonId", lessonId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_completion)
        val moduleId = intent.getIntExtra("moduleId", 0)
        val lessonId = intent.getIntExtra("lessonId", 0)
        val module = ModulesDataStore.loadModule(moduleId)
        val vm = CompletionVm(moduleId, module!!, lessonId)
        binding.vm = vm
        binding.buttonNextRound.setOnClickListener {
            if (vm.shouldStartNextRound) {
                startActivity(ClassroomActivity.createIntent(this, moduleId, lessonId + 1))
            }
            finish()
        }
        val progressValue = (((lessonId.toFloat() + 1) / (vm.lessonsSize).toFloat()) * 100).toInt()
        binding.moduleProgress.setProgress(progressValue)
    }
}

class CompletionVm(moduleId: Int, module: Module, lessonId: Int) {
    val moduleImage = ModulesDataStore.moduleImage(moduleId)
    val moduleImageBackground = ModulesDataStore.moduleImageBgColor(moduleId)
    val moduleName = module.title
    val lessonsSize = module.lessons.size
    val currentlessonid = lessonId + 1
    val completionText = "$currentlessonid/$lessonsSize completed"
    val shouldStartNextRound = (lessonId + 1) < lessonsSize
}