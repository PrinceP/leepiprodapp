package com.mangoai.leepi

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.mangoai.leepi.databinding.ActivitySplashBinding
import com.mangoai.leepi.home.HomeActivity
import timber.log.Timber

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)
        val videoPath = "android.resource://$packageName/" + R.raw.leepi_intro
        binding.splashStart.setVideoURI(Uri.parse(videoPath))
        binding.splashStart.setOnPreparedListener {
            binding.splashStart.start()
        }
        binding.splashStart.setOnErrorListener { mp, what, extra ->
            Timber.e("Error occurred")
            startHomeActivityAndFinish()
            false
        }
        binding.splashStart.setOnCompletionListener {
            startHomeActivityAndFinish()
        }
    }

    private fun startHomeActivityAndFinish() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}