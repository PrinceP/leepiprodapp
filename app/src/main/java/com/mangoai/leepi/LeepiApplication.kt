package com.mangoai.leepi

import android.app.Application
import com.getkeepsafe.relinker.ReLinker
import com.google.mediapipe.framework.AndroidAssetUtil
import com.mangoai.leepi.util.ReleaseTree
import timber.log.Timber

class LeepiApplication: Application() {
    var isSplashViewed = false
    companion object {
        lateinit var instance: LeepiApplication
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Timber.plant(
            if (BuildConfig.DEBUG) Timber.DebugTree()
            else ReleaseTree()
        )
        ReLinker.loadLibrary(this, "mediapipe_jni", object: ReLinker.LoadListener {
            override fun success() {
                Timber.e("Mediapipe Loaded Successfully")
                // Initialize asset manager so that MediaPipe native libraries can access the app assets, e.g.,
                // binary graphs.
                AndroidAssetUtil.initializeNativeAssetManager(this@LeepiApplication)
            }

            override fun failure(t: Throwable?) {

            }

        })
        ReLinker.loadLibrary(this, "opencv_java3", object: ReLinker.LoadListener {
            override fun success() {
                Timber.e("OpenCV Loaded Successfully")
            }

            override fun failure(t: Throwable?) {

            }

        })

    }
}