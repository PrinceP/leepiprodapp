package com.mangoai.leepi

import com.google.mediapipe.framework.AndroidAssetUtil
import com.mangoai.leepi.models.Module
import com.mangoai.leepi.models.Modules
import com.mangoai.leepi.models.Modules__JsonHelper

const val BEGINNER = 1
const val OBJECTS = 2
const val FOOD = 3
const val COMMON = 4
const val GREETINGS = 5
const val FAMILY = 6
const val TIME = 7
const val PLACES = 8

object ModulesDataStore {
    private val context = LeepiApplication.instance

    private val modules = loadModulesJson()

    private val moduleNames = mapOf(
        BEGINNER to "Beginner",
        OBJECTS to "Objects",
        FOOD to "Food",
        COMMON to "Common",
        GREETINGS to "Greetings",
        FAMILY to "Family",
        TIME to "Time",
        PLACES to "Places"
    )

    private val moduleImages = mapOf(
        BEGINNER to R.drawable.ic_beginner,
        OBJECTS to R.drawable.ic_objects,
        FOOD to R.drawable.ic_food,
        COMMON to R.drawable.ic_common,
        GREETINGS to R.drawable.ic_greetings,
        FAMILY to R.drawable.ic_family,
        TIME to R.drawable.ic_alarm,
        PLACES to R.drawable.ic_map
    )

    private val moduleImagesBackgroundColorRes = mapOf(
        BEGINNER to R.color.purple_heart,
        OBJECTS to R.color.yellow_400,
        FOOD to R.color.deep_cerulean,
        COMMON to R.color.indigo_800,
        GREETINGS to R.color.purple_heart,
        FAMILY to R.color.yellow_400,
        TIME to R.color.deep_cerulean,
        PLACES to R.color.indigo_800
    )

    private val letterImageMap = mapOf(
        "A" to R.drawable.a,
        "B" to R.drawable.b,
        "C" to R.drawable.c,
        "D" to R.drawable.d,
        "E" to R.drawable.e,
        "H" to R.drawable.h,
        "I" to R.drawable.i,
        "K" to R.drawable.k,
        "N" to R.drawable.n,
        "O" to R.drawable.o,
        "P" to R.drawable.p,
        "R" to R.drawable.r,
        "S" to R.drawable.s,
        "T" to R.drawable.t,
        "U" to R.drawable.u,
        "W" to R.drawable.w
    )

    private val wordAnimMap = mapOf(
        "Book" to R.raw.book_anim,
        "Candy" to R.raw.candy_anim,
        "Clothes" to R.raw.clothes_anim,
        "Computer" to R.raw.computer_anim,
        "Drink" to R.raw.drink_anim,
        "Want" to R.raw.want_anim
    )

    fun loadModulesJson(): Modules {
        val buffer = AndroidAssetUtil.getAssetBytes(context.assets, "modules.json")
        val modulesJson = String(buffer)
        return Modules__JsonHelper.parseFromJson(modulesJson)
    }

    fun moduleImage(moduleId: Int): Int = moduleImages[moduleId] ?: 0

    fun moduleName(moduleId: Int): String = moduleNames[moduleId] ?: "Beginner"

    fun moduleImageBgColor(moduleId: Int): Int =
        moduleImagesBackgroundColorRes[moduleId] ?: R.color.purple_heart

    fun loadModule(moduleId: Int): Module? {
        return modules.moduleList.firstOrNull {
            it.id == moduleId
        }
    }

    fun imageForLetter(letter: String) = letterImageMap[letter]

    fun animationForWord(word: String) = wordAnimMap[word]
}