package com.mangoai.leepi.models;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

@JsonType
class Media {
    @JsonField(fieldName="type")
     String type;

    @JsonField(fieldName="url")
     String url = "NA";

}
