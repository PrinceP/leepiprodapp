package com.mangoai.leepi.models;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

@JsonType
public class Module {
    @JsonField(fieldName = "title")
    public String title;

    @JsonField(fieldName = "media")
    public Media media;

    @JsonField(fieldName = "id")
    public int id;

    @JsonField(fieldName = "locked")
    public boolean locked;

    @JsonField(fieldName = "lessons")
    public List<Lesson> lessons;

    @Override
    public String toString() {
        return "Module{" +
                "title='" + title + '\'' +
                ", id=" + id +
                '}';
    }
}
