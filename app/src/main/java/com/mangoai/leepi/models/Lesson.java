package com.mangoai.leepi.models;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;
@JsonType
public class Lesson {
    @JsonField(fieldName="id")
    public int id;

    @JsonField(fieldName="media")
    public Media media;

    @JsonField(fieldName="title")
    public String title;

    @JsonField(fieldName="content")
    public List<Content> contents;
}
