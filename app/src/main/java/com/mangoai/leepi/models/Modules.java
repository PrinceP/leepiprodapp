package com.mangoai.leepi.models;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

import java.util.List;

@JsonType
public class Modules {

    @JsonField(fieldName="modules")
    public List<Module> moduleList;

    @Override
    public String toString() {
        return "Modules{" +
                "moduleList=" + moduleList.get(0).toString() +
                '}';
    }
}

