package com.mangoai.leepi.models;

import com.instagram.common.json.annotation.JsonField;
import com.instagram.common.json.annotation.JsonType;

@JsonType
public class Content {
    @JsonField(fieldName="title")
    public String title;

    @JsonField(fieldName="media")
    public Media media;

    @JsonField(fieldName="type")
    public String type;
}
