package com.mangoai.leepi.bindings

import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter


@BindingAdapter("android:visibility")
fun bindVisibility(view: View, isVisible: Boolean) {
    val visibility = if (isVisible) View.VISIBLE else View.GONE
    view.visibility = visibility
}

@BindingAdapter(value = ["android:visibility", "animateFadeIn"])
fun bindVisibilityWithAnimateFadeIn(view: View, isVisible: Boolean, animate: Boolean) {
    val visibility = if (isVisible) View.VISIBLE else View.GONE
    if (animate && isVisible) {
        view.visibility = visibility
        view.alpha = 0f
        view.animate().apply {
            alpha(1f)
            setListener(null)
            interpolator = AccelerateDecelerateInterpolator()
        }.start()
    } else {
        view.alpha = 1f
        view.visibility = visibility
    }
}


@BindingAdapter("textColorRes")
fun bindTextColorRes(view: TextView, @ColorRes colorRes: Int) {
    view.setTextColor(ContextCompat.getColor(view.context, colorRes))
}

@BindingAdapter("cardBackgroundColorRes")
fun bindCardBackgroundColor(cardView: CardView, @ColorRes colorRes: Int) {
    cardView.setCardBackgroundColor(ContextCompat.getColor(cardView.context, colorRes))
}

@BindingAdapter("android:src")
fun bindImageResource(imageView: ImageView, resId: Int) {
    imageView.setImageResource(resId)
}

@BindingAdapter("backgroundColorRes")
fun bindBackgroundColorRes(view: View,@ColorRes colorRes: Int) {
    view.setBackgroundColor(ContextCompat.getColor(view.context, colorRes))
}