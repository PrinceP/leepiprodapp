# Leepi Production Application - Android #

***  Android application built using Kotlin, Tensorflow Lite and Mediapipe dependencies.  ***


### Features
- [x] Hand Detection 
- [x] Hand Box Classification for Letters in ASL
- [x] Hand Gesture Classification for Words in ASL
- [ ] Words and Letters mediafiles on Firebase 


### Dependencies
* libs/multi_hand_tracking_display_aar 
*     5B Mar 26 10:35 palm_detection_labelmap.txt
*   384B Mar 26 10:35 .
*   6.8M Mar 26 10:35 palm_detection.tflite
*    10K Mar 26 10:35 modules.json
*    13M Mar 26 10:35 letters.tflite
*   413K Mar 26 10:35 face_detection_front.tflite
*    11M Mar 26 10:35 hand_landmark.tflite
*    47B Mar 26 10:35 letters.txt
*   882K Mar 26 10:35 one_15.tflite
*     5B Mar 26 10:35 face_detection_front_labelmap.txt
*   2.0K Mar 26 10:35 multihandtrackinggpu.binarypb 


### How do I get set up? ###

* Clone the repository. 
* Open in Android Studio. 

### Training models ###

[Training](https://bitbucket.org/PrinceP/training/src/main/)

### Contribution guidelines ###

* Open Issues for new features and bug fixes

### Who do I talk to? ###

* prince.patel.14@gmail.com